<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>All Assignment</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th>
                    <th><input type="checkbox" id="check-all" class="flat"></th>
                  </th>
                  <th>Class ID</th>
                  <th>Subject Name</th>
                  <th>Note</th>
                  <th>Assignment</th>
                  <th>Submission</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>
                    <th><input type="checkbox" id="check-all" class="flat"></th>
                  </td>
                  <td>BIT306</td>
                  <td>Web Technologies</td>
                  <td>Assignment 1</td>
                  <td>
                  <button type="button" class="btn btn-warning btn-xs">View</button>
                  </td>
                  <td>
                  <a href="upload_assignment.html"><button type="button" class="btn btn-success btn-xs">Submit</button></a>
                  </td>
                </tr>
                <tr>
                 <td>
                   <th><input type="checkbox" id="check-all" class="flat"></th>
                 </td>
                  <td>BIT306</td>
                  <td>Web Technologies</td>
                  <td>Assignment 2</td>
                  <td>
                  <button type="button" class="btn btn-warning btn-xs">View</button>
                  </td>
                  <td>
                  <a href="upload_assignment.html"><button type="button" class="btn btn-success btn-xs">Submit</button></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>
<!-- /page content -->


<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
