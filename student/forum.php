<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Forum E-Learning</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <!-- start project list -->
            <table class="table table-striped projects">
              <thead>
                <tr>
                  <th style="width: 1%">#</th>
                  <th>Forum Name</th>
                  <th style="width: 10%">Threads</th>
                  <th style="width: 20%">Last post</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>#</td>
                  <td>
                    <b><a href="#"> Let's chit-chat in a Fun Way </a></b>
                    <br />
                    <small>Let's chat anything here.</small>
                  </td>
                  <td align="center">
                    <a>1</a>
                  </td>
                  <td>
                    <small>01.01.2018, 00:00</small>
                    <br />
                    <small>Justin Banu Prabowo</small>
                  </td>
                </tr>
                <tr>
                  <td>#</td>
                  <td>
                    <b><a href="#"> PHP </a></b>
                    <br />
                    <small>Sharing about PHP here.</small>
                  </td>
                  <td align="center">
                    <a>1</a>
                  </td>
                  <td>
                    <small>01.01.2018, 00:00</small>
                    <br />
                    <small>Komang Rinartha</small>
                  </td>
                </tr>
                <tr>
                  <td>#</td>
                  <td>
                    <b><a href="#"> Laravel </a></b>
                    <br />
                    <small>Sharing about Laravel.</small>
                  </td>
                  <td align="center">
                    <a>1</a>
                  </td>
                  <td>
                    <small>01.01.2018, 00:00</small>
                    <br />
                    <small>SidiSan</small>
                  </td>
                  <tr>
                    <td>#</td>
                    <td>
                      <b><a href="#"> Vue.JS </a></b>
                      <br />
                      <small>Sharing about Vue.JS</small>
                    </td>
                    <td align="center">
                      <a>1</a>
                    </td>
                    <td>
                      <small>01.01.2018, 12:00</small>
                      <br />
                      <small>Justin Banu Prabowo</small>
                    </td>
                  </tr>
                  <tr>
                    <td>#</td>
                    <td>
                      <b><a href="#"> Bootstrap </a></b>
                      <br />
                      <small>Sharing about Bootstrap</small>
                    </td>
                    <td align="center">
                      <a>1</a>
                    </td>
                    <td>
                      <small>01.01.2018, 12:00</small>
                      <br />
                      <small>Zefi Bulkiah</small>
                    </td>
                  </tr>
                </tr>
              </tbody>
            </table>
            <!-- end project list -->
          </div>
        </div>
      </div>
</div>
<!-- /page content -->


<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
