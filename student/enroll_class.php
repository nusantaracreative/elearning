<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Enroll in a class</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" data-parsley-validate >

              <p>To enroll a class, enter the <code>Class ID</code> and <code>Enrollment Key</code> and click submit. If you do not have a class ID and enrollment key, contact your instructor for this information.</a>
              </p>
              <span class="section"></span>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Class ID: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="idclass" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Enrollment Key: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="enroll_key" required="required" type="text">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>
            <?php
              if (isset($_POST["idclass"])) {
                $Gidclass=$_POST["idclass"];
                $Gtb_user_username=$_SESSION['username'];
                $Genroll_key=$_POST["enroll_key"];
                $query = $conn->prepare("SELECT * FROM class WHERE BINARY idclass = ?");

                //cek class id di database
                $query->execute(array($Gidclass));
                $hasil = $query->fetch();
                if($query->rowCount() == 0){
                    echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                </button>
                                <strong>Alerts</strong> Class ID not registered!
                            </div>";
                } else {
                //cek apabila ada perbedaan enroll key di database
                    if($Genroll_key <> $hasil['enroll_key']){
                        echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                    </button>
                                    <strong>Alerts</strong> Wrong enrollment key!
                                </div>";
                    } else {
                      $sqlEnroll = "INSERT INTO enroll_class(class_idclass, tb_user_username)
                        VALUES ('$Gidclass','$Gtb_user_username')";

                        $count=0;
                        foreach($conn->query('SELECT * FROM enroll_class') as $row){
                          if ($row['class_idclass']==$Gidclass && $row['tb_user_username']==$Gtb_user_username) {
                            $count=$count+1;
                          }
                        }

                        if($count>0){
                          echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                  </button>
                                  <strong>Alerts</strong> You are already enrolled in this class.
                                </div>";
                        }else{
                          $sqlEnroll=$conn->exec($sqlEnroll);
                          if ($sqlEnroll== TRUE) {
                            echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                    </button>
                                    <strong>Success</strong> You're enrolled to the class.
                                  </div>";
                          } else {
                            echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                    </button>
                                    <strong>Alerts</strong> Error!
                                  </div>";
                                }
                              }
                            }
                          }
                        }
              ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->


<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
