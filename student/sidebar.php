<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-university"></i> Classes <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="index.php">All classes</a></li>
          <li><a href="enroll_class.php">Enroll in a Class</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Quizzes <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="all_quizzes.php">All quizzes</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-folder"></i> Assignment <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="all_assignment.php">All Assignment</a></li>
        </ul>
      </li>
      <li><a href="forum.php"><i class="fa fa-desktop"></i> Forum </a></li>
      </li>
    </ul>
  </div>
</div>
<!-- /sidebar menu -->
