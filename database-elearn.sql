-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bit306_e1200261
DROP DATABASE IF EXISTS `bit306_e1200261`;
CREATE DATABASE IF NOT EXISTS `bit306_e1200261` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bit306_e1200261`;

-- Dumping structure for table bit306_e1200261.class
DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `idclass` varchar(50) NOT NULL,
  `tb_user_username` varchar(50) NOT NULL,
  `subject_name` varchar(50) DEFAULT NULL,
  `enroll_key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idclass`),
  KEY `class_FKIndex1` (`tb_user_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bit306_e1200261.class: ~0 rows (approximately)
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` (`idclass`, `tb_user_username`, `subject_name`, `enroll_key`) VALUES
	('BIT306', 'komang', 'Web Technologies', 'WebTech2017'),
	('kelas', 'lecture', 'Subject 1', 'enrol'),
	('kelas1', 'lecture', 'Nama', 'enrol'),
	('kelas2', 'lecture', 'subjek lagi', 'er');
/*!40000 ALTER TABLE `class` ENABLE KEYS */;

-- Dumping structure for table bit306_e1200261.enroll_class
DROP TABLE IF EXISTS `enroll_class`;
CREATE TABLE IF NOT EXISTS `enroll_class` (
  `idenroll` int(11) NOT NULL AUTO_INCREMENT,
  `tb_user_username` varchar(50) DEFAULT '0',
  `class_idclass` varchar(50) DEFAULT '0',
  PRIMARY KEY (`idenroll`),
  KEY `tb_user_username` (`tb_user_username`),
  KEY `class_idclass` (`class_idclass`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bit306_e1200261.enroll_class: ~0 rows (approximately)
/*!40000 ALTER TABLE `enroll_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `enroll_class` ENABLE KEYS */;

-- Dumping structure for table bit306_e1200261.profile
DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id` varchar(50) NOT NULL,
  `tb_user_username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_FKIndex1` (`tb_user_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bit306_e1200261.profile: ~0 rows (approximately)
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`id`, `tb_user_username`, `name`, `email`) VALUES
	('14045', 'komang', 'Komang Rinartha', 'komang@rinartha.com');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;

-- Dumping structure for table bit306_e1200261.quizorassignment
DROP TABLE IF EXISTS `quizorassignment`;
CREATE TABLE IF NOT EXISTS `quizorassignment` (
  `idQnA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_idclass` varchar(50) NOT NULL,
  `tb_user_username` varchar(50) DEFAULT NULL,
  `QorA` tinyint(3) unsigned DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idQnA`),
  KEY `QuizOrAssignment_FKIndex1` (`class_idclass`),
  KEY `QuizOrAssignment_FKIndex2` (`tb_user_username`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table bit306_e1200261.quizorassignment: ~7 rows (approximately)
/*!40000 ALTER TABLE `quizorassignment` DISABLE KEYS */;
INSERT INTO `quizorassignment` (`idQnA`, `class_idclass`, `tb_user_username`, `QorA`, `note`, `filename`) VALUES
	(12, 'kelas', 'lecture', 0, 'asdasd', '1516211262.docx'),
	(13, 'kelas', 'lecture', 0, 'note2', '1516212159.docx'),
	(14, 'kelas2', 'lecture', 0, 'note1', '1516212243.docx'),
	(15, 'kelas', 'lecture', 1, 'quiz 1', '1516212480.docx'),
	(16, 'kelas1', 'lecture', 1, 'quiz 1', '1516212491.docx'),
	(17, 'BIT306', 'komang', 0, 'Assignment 1', '1516389450.docx'),
	(18, 'BIT306', 'komang', 1, 'Quiz 1', '1516389973.docx');
/*!40000 ALTER TABLE `quizorassignment` ENABLE KEYS */;

-- Dumping structure for table bit306_e1200261.tb_user
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE IF NOT EXISTS `tb_user` (
  `username` varchar(50) NOT NULL,
  `user_pass` varchar(50) DEFAULT NULL,
  `user_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bit306_e1200261.tb_user: ~1 rows (approximately)
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` (`username`, `user_pass`, `user_type`) VALUES
	('komang', 'komang', 'teacher'),
	('lecture', 'lecture', 'teacher'),
	('student', 'student', 'student');
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
