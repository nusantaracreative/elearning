<?php
    require_once("connect.php");
    session_start();
    if(isset($_SESSION['user_type']))
       {
           if ($_SESSION['user_type']=='student'){
                header("Location:student/index.php");
           } else if($_SESSION['user_type']=='teacher'){
                header("Location:teacher/index.php");
           } else if($_SESSION['user_type']=='admin'){
                header("Location:admin/index.php");
           }
       }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>E-Learning </title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <?php
            if (isset($_POST["username"])) {
                    $username = $_POST['username'];
                    $pass = $_POST['user_pass'];
                    $query = $conn->prepare("SELECT * FROM tb_user WHERE username = ?");

                    //cek username di database
                    $query->execute(array($username));
                    $hasil = $query->fetch();
                    if($query->rowCount() == 0){
                        echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                    </button>
                                    <strong>Alerts</strong> Username not registered!
                                </div>";
                    } else {
                    //cek apabila ada perbedaan password di database
                        if($pass <> $hasil['user_pass']){
                            echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                        </button>
                                        <strong>Alerts</strong> Wrong Password!
                                    </div>";
                        } else {
                            $level = $hasil['level'];
                            $_SESSION['username'] = $hasil['username'];
                            $_SESSION['user_type'] = $hasil['user_type'];
                            if ($_SESSION['user_type']=='student'){
                               header("Location:student/index.php");
                               }
                              else if($_SESSION['user_type']=='teacher'){
                               header("Location:teacher/index.php");
                               }
                              else if($_SESSION['user_type']=='admin'){
                               header("Location:admin/index.php");
                              }
                            }
                        }
            }

            if (isset($_POST["Occupation"])) {
              $username = $_POST['Regis_username'];
              $email = $_POST['email'];
              $password = $_POST['Regis_password'];
              $name = $_POST['name'];
              $occupation = $_POST["Occupation"];
              $Regis_ID = $_POST["Regis_ID"];

              $UserQuery = $conn->prepare("SELECT * FROM tb_user WHERE username = ?");
              $UserQuery->execute(array($username));

              $ProfileQuery = $conn->prepare("SELECT * FROM profile WHERE id = ?");
              $ProfileQuery->execute(array($Regis_ID));

              if($UserQuery->rowCount() > 0){
                  echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                              </button>
                              <strong>Alerts</strong> Username already registered!
                          </div>";
              }elseif($ProfileQuery->rowCount() > 0){
                  echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                              </button>
                              <strong>Alerts</strong> ID already registered!
                          </div>";

              }else{
                $sqlUser="INSERT INTO tb_user(username,user_pass,user_type)
                  VALUES('$username','$password','$occupation')";
                $execUser=$conn->exec($sqlUser);
                if ($execUser == TRUE) {
                  $sqlProfile="INSERT INTO profile(id,tb_user_username,name,email)
                    VALUES('$Regis_ID','$username','$name','$email')";
                  $execProfile=$conn->exec($sqlProfile);
                  if ($execProfile == TRUE) {
                    echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                            </button>
                            <strong>Success</strong> Account has been created.
                          </div>";
                  } else {
                    echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                              </button>
                              <strong>Alerts</strong> Something Error!
                          </div>";
                  }

                } else {
                  echo "  <div class='alert alert-danger alert-dismissible fade in' role='alert'>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                              </button>
                              <strong>Alerts</strong> Creating User!
                          </div>";
                }

              }
            }
        ?>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="#signin" method="post">
              <h1>Login E-Learning</h1>
              <div>
                <input name="username" type="text" class="form-control" placeholder="Username" required autofocus />
              </div>
              <div>
                <input name="user_pass" type="password" class="form-control" placeholder="Password" required />
              </div>
              <div>
                <div class="row">
                      <div class="btn-group" data-toggle="buttons">
                        <button type="submit" class="btn btn-default submit">Login</button>
                      </div>
                    </div>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <p>©2017 All Rights Reserved.</p>
                </div>
              </div>
            </form>
          </section>
        </div>
        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form action="#signup" method="post">
              <h1>Create Account</h1>

              <div>
                <select name="Occupation" class="btn btn-default dropdown-toggle login">
                  <option value="#" disabled>Select Occupation</option>
                  <option value="student">Student</option>
                  <option value="teacher">Lecture</option>
                </select>
              </div>
              <div>
                <input name="Regis_ID" type="text" class="form-control" placeholder="ID" required="" />
              </div>
              <div>
                <input name="name" type="text" class="form-control" placeholder="Name" required="" />
              </div>
              <div>
                <input name="Regis_username" type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input name="email" type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input name="Regis_password" type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">submit</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <p>©2017 All Rights Reserved.</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
