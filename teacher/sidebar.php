<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-university"></i> Classes <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="index.php">All Classes</a></li>
          <li><a href="create_class.php">Create Class</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Quizzes <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="manage_quiz.php">All quizzes</a></li>
          <li><a href="create_quiz.php">Create Quiz</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-folder"></i> Assignment <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="manage_assignment.php">All Assignment</a></li>
          <li><a href="create_assignment.php">Create Assignment</a></li>
        </ul>
      </li>
      <li><a href="forum.php"><i class="fa fa-desktop"></i> Forum </a></li>
    </ul>
  </div>
</div>
<!-- /sidebar menu -->
