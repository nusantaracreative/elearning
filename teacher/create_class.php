<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Create a Class</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" data-parsley-validate >
              <p>To create a class, enter the <code>Class ID</code>, <code>Enrollment Key</code>, <code>Subject Name</code> and click submit.</a>
              </p>
              <span class="section"></span>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Class ID: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="idclass" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Enrollment Key: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="enroll_key" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject Name: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="subject_name" required="required" type="text">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>
            <?php
                  if (isset($_POST["idclass"])) {
                    $Gidclass=$_POST["idclass"];
                    $Genroll_key=$_POST["enroll_key"];
                    $Gsubject_name=$_POST["subject_name"];
                    $Gtb_user_username=$_SESSION['username'];

                    $sqlClass = "INSERT INTO class(idclass, tb_user_username, enroll_key, subject_name)
                      VALUES ('$Gidclass','$Gtb_user_username','$Genroll_key','$Gsubject_name')";

                    $count=0;
                    foreach($conn->query('SELECT idclass FROM class') as $row){
                      if ($row['idclass']==$Gidclass) {
                        $count=$count+1;
                      }
                    }
                    if($count>0){
                      echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                    </button>
                                    <strong>Alerts</strong> Class ID already exist.
                                  </div>";
                                }else{
                                  $conn->exec($sqlClass);
                          echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                  </button>
                                  <strong>Success</strong> Class created.
                                </div>";
                                }
                  }
                  $conn = null;
                ?>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
