<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Create an Assignment</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" data-parsley-validate >
              <p>To create an Assignment, select a <code>class</code> and <code>write a Note</code> and <code>Upload the Assignment</code> and click submit.</a>
              </p>
              <span class="section"></span>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Class: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="select2_single form-control" name="idclass">
                    <option></option>
                    <?php
                    $Gtb_user_username=$_SESSION['username'];
                    $sql="SELECT idclass,subject_name FROM class WHERE tb_user_username='$Gtb_user_username'";
                    foreach ($conn->query($sql) as $row) {
                      ?>
                    <option value="<?php echo $row['idclass'];?>"><?php echo $row['subject_name'];?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Note: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="note" required="required" type="text">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Upload Assignment: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" data-role="magic-overlay" data-target="#pictureBtn" name="fileToUpload" data-edit="insertImage" />
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>
            <?php
            if (isset($_POST["idclass"])) {
              $Gidclass=$_POST["idclass"];
              $Gnote=$_POST["note"];
              $GUsername=$_SESSION['username'];
              $QorA=0;

              $target_dir = "../files/";
              $temp = explode(".", $_FILES["fileToUpload"]["name"]);
              $newfilename = round(microtime(true)) . '.' . end($temp);
              $target_file = $target_dir . $newfilename;
              $uploadOk = 1;
              $FileType = pathinfo($target_file,PATHINFO_EXTENSION);
              try{

                // Check file size
                if ($_FILES["fileToUpload"]["size"] > 2500000) {
                  echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                </button>
                                <strong>Alerts</strong> Sorry, your file is too large..
                              </div>";
                    $uploadOk = 0;
                }

                // Allow certain file formats
                if($FileType != "pdf" && $FileType != "docx" && $FileType != "doc" ) {
                  echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                </button>
                                <strong>Alerts</strong> Sorry, only PDF, DOC & DOCX files are allowed..
                              </div>";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                  echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                                </button>
                                <strong>Alerts</strong> Sorry, your file was not uploaded..
                        </div>";

                // if everything is ok, try to upload file
                } else {
                  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    $sqlClass = "INSERT INTO quizorassignment(class_idclass, tb_user_username, QorA, note, filename) VALUES ('$Gidclass','$GUsername','$QorA','$Gnote','$newfilename')";
                    $conn->exec($sqlClass);
                    echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                            </button>
                            <strong>Success</strong> Assignment created.
                          </div>";
                  } else {
                    echo "Sorry, there was an error uploading your file.";
                  }
                }
            }
            catch(PDOException $e){
              echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                            </button>
                            <strong>Alerts</strong> Error.
                          </div>";
            }
          }
          ?>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
