<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Quiz 1 - BIT306 - Web Technologies</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
              <thead>
                <tr>
                  <th>
                    <th><input type="checkbox" id="check-all" class="flat"></th>
                  </th>
                  <th>Student ID</th>
                  <th>Student Name</th>
                  <th>Score</th>
                  <th>Quiz</th>
                  <th>Assesment</th>
                </tr>
              </thead>


              <tbody>
                <tr>
                  <td>
                    <th><input type="checkbox" id="check-all" class="flat"></th>
                  </td>
                  <td>E1200312</td>
                  <td>Justin Banu Prabowo Manik</td>
                  <td>100</td>
                  <td>
                  <button type="button" class="btn btn-warning btn-xs">View</button>
                  </td>
                  <td>
                  <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm">Assign Score</button>
                  </td>
                </tr>
                <tr>
                 <td>
                   <th><input type="checkbox" id="check-all" class="flat"></th>
                 </td>
                  <td>E1200311</td>
                  <td>Sidi Meisanjaya</td>
                  <td>100</td>
                  <td>
                  <button type="button" class="btn btn-warning btn-xs">View</button>
                  </td>
                  <td>
                  <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm">Assign Score</button>
                  </td>
                </tr>
                <tr>
                 <td>
                   <th><input type="checkbox" id="check-all" class="flat"></th>
                 </td>
                  <td>E1200310</td>
                  <td>Disteryan Julio</td>
                  <td>100</td>
                  <td>
                  <button type="button" class="btn btn-warning btn-xs">View</button>
                  </td>
                  <td>
                  <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm">Assign Score</button>

                  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Assign Score</h4>
                        </div>
                        <div class="modal-body">
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Score:
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="name" class="form-control col-md-7 col-xs-12" placeholder="e.g 95" required="required" type="text">
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save</button>
                        </div>

                      </div>
                    </div>
                  </div>

                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>
<!-- /page content -->


<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
