<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit Class</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <?php
            if (isset($_POST["idclass"])) {
              $Gidclass=$_POST["idclass"];
              $Genroll_key=$_POST["enroll_key"];
              $Gsubject_name=$_POST["subject_name"];
              $Gtb_user_username=$_SESSION['username'];

              $sqlClass = "UPDATE class SET enroll_key ='$Genroll_key', subject_name='$Gsubject_name'
                WHERE idclass='$Gidclass'";

              $ExeClass=$conn->exec($sqlClass);
              if ($ExeClass== TRUE) {
                echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                        </button>
                        <strong>Success</strong> Class Updated.
                      </div>";
              } else {
                echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                        </button>
                        <strong>Alerts</strong> Error!
                      </div>";
              }
            }

            if (isset($_GET["idcl"])) {
              $idclass=$_GET["idcl"];
              $sql="SELECT * FROM class WHERE idclass='$idclass'";
            }
            foreach($conn->query($sql) as $row){
            ?>
            <form class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" data-parsley-validate >
              <p>To edit class, by update <code>Enrollment Key</code>, <code>Subject Name</code> and click submit.</a>
              </p>
              <span class="section"></span>

              <div class="item form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="idclass" required="required" type="hidden" value="<?php echo
                  $row['idclass'] ?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Enrollment Key: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="enroll_key" required="required" type="text" value="<?php echo
                  $row['enroll_key'] ?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subject Name: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="subject_name" required="required" type="text" value="<?php echo
                  $row['subject_name'] ?>">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
