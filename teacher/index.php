<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>All Classes</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <!-- start project list -->
            <table class="table table-striped projects">
              <thead>
                <tr>
                  <th style="width: 1%">#</th>
                  <th>Class ID</th>
                  <th>Subject Name</th>
                  <th>Quiz</th>
                  <th>Assignment</th>
                  <th>Enrollment Key</th>
                  <th style="width: 20%">#Action</th>
                </tr>
              </thead>

              <tbody>
                <?php
                  $GUsername=$_SESSION['username'];
                  foreach($conn->query("SELECT * FROM class WHERE tb_user_username = '$GUsername'") as $row) {
                ?>
                <tr>
                  <td>#</td>
                  <td>
                    <a><?php echo $row['idclass']; ?></a>
                  </td>
                  <td>
                    <a><?php echo $row['subject_name']; ?></a>
                  </td>
                  <td>
                    <a href="view_quiz.php"><button type="button" class="btn btn-warning btn-xs">View Quiz</button></a>
                  </td>
                  <td>
                    <a href="view_assignment.php"><button type="button" class="btn btn-danger btn-xs">View Assignment</button></a>
                  </td>
                  <td>
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm">View</button>
                  </td>
                  <td>
                    <a href="edit_class.php?idcl=<?php echo $row['idclass'];?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
            <!-- end project list -->
          </div>
        </div>
      </div>
</div>
<!-- /page content -->


<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
