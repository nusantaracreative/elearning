<!-- page header -->
<?php include ("header.php"); ?>
<!-- page header -->


<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit Assignment</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <?php
            if (isset($_POST["idQnA"])) {
              $GidQnA=$_POST["idQnA"];
              $Gnote=$_POST["note"];

              $sqlClass = "UPDATE quizorassignment SET note ='$Gnote'
                WHERE idQnA='$GidQnA'";

              $ExeClass=$conn->exec($sqlClass);
              if ($ExeClass== TRUE) {
                echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                        </button>
                        <strong>Success</strong> Assignment Updated.
                      </div>";
              } else {
                echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                        </button>
                        <strong>Alerts</strong> Error!
                      </div>";
              }
            }

            if (isset($_GET["idass"])) {
              $GidQnA=$_GET["idass"];
              $sql="SELECT * FROM quizorassignment WHERE idQnA='$GidQnA'";
            }
            foreach($conn->query($sql) as $row){
            ?>
            <form class="form-horizontal form-label-left" action="#" method="post" enctype="multipart/form-data" data-parsley-validate >
              <p>To edit Quiz, by edit <code>Note</code> and click submit.</a>
              </p>
              <span class="section"></span>

              <div class="item form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="idQnA" required="required" type="hidden" value="<?php echo
                  $row['idQnA'] ?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Note: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="0" name="note" required="required" type="text" value="<?php echo
                  $row['note'] ?>">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- /page content -->

<!-- page footer -->
<?php include ("footer.php"); ?>
<!-- /page footer -->
